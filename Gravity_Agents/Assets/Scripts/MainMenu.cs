using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour, IScenes, IQuit
{
    public void StartGame()
    {
        SceneManager.LoadScene(IScenes.Name.Level_1.ToString());
    }
    public void StartTraining()
    {
        SceneManager.LoadScene(IScenes.Name.TrainingLevel.ToString());
    }
    public void QuitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
