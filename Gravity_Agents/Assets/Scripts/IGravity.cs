using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGravity
{
    bool IsGrounded();
    void GravitySwitch();
    void Rotate();
    void Flip();
    void Spawn();
}
