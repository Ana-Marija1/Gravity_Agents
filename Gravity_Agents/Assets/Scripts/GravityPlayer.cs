using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GravityPlayer : MonoBehaviour, IScenes, IGravity
{
    [SerializeField] private Transform target;
    [SerializeField] private LayerMask platformLayer;
    [SerializeField] private TrapManager trapManager;
    [SerializeField] private SpawnPointManager spawnPointManager;
    [SerializeField] private MiddleSectionManager middleSectionManager;
    [SerializeField] private GravityAgent agent = null;
    [SerializeField] private Animator animator;
    private new Rigidbody2D rigidbody;
    private BoxCollider2D boxCollider;
    private bool top = false;
    private bool facingRight = true;
    private float speed = 1f;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        trapManager.SetTrapsLevel();
        target.localPosition = new Vector3(Random.Range(202, 207), 0);
        middleSectionManager.ChooseMiddleSection();
        Spawn();
        transform.localPosition = spawnPointManager.ChooseSpawnPoint();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ResetAgent();
            SceneManager.LoadScene(IScenes.Name.MainMenu.ToString());
        }
        animator.SetBool("IsRunning", false);
        if (IsGrounded())
        {
            if (Input.GetKey(KeyCode.A))
            {
                if (facingRight)
                {
                    Flip();
                }
                transform.localPosition += Vector3.left * speed * Time.deltaTime;
                animator.SetBool("IsRunning", true);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                if (!facingRight)
                {
                    Flip();
                }
                transform.localPosition += Vector3.right * speed * Time.deltaTime;
                animator.SetBool("IsRunning", true);
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                GravitySwitch();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Target"))
            switch (SceneManager.GetActiveScene().name)
            {
                case "Level_1":
                    SceneManager.LoadScene("Level_2");
                    break;
                case "Level_2":
                    SceneManager.LoadScene("Level_3");
                    break;
                default:
                    SceneManager.LoadScene("MainMenu");
                    break;
            };
        if (collider.gameObject.CompareTag("Border") || collider.gameObject.CompareTag("Trap"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        ResetAgent();
    }
    private void ResetAgent()
    {
        agent?.Reset();
    }
    public bool IsGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, 0.4f, platformLayer);
        if (top == true)
        {
            raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.up, 0.4f, platformLayer);
        }
        return raycastHit.collider != null;
    }
    public void GravitySwitch()
    {
        rigidbody.gravityScale *= -1;
        Rotate();
        facingRight = !facingRight;
        Flip();
    }
    public void Rotate()
    {
        transform.localEulerAngles = top ? Vector3.zero : new Vector3(0, 0, 180f);
        top = !top;
    }
    public void Flip()
    {
        facingRight = !facingRight;
        if (top == false)
        {
            transform.localEulerAngles = facingRight ? Vector3.zero : new Vector3(0, 180f, 0);
        }
        else
        {
            transform.localEulerAngles = facingRight ? new Vector3(0, 180f, 180f) : new Vector3(0, 0, 180f);
        }
    }
    public void Spawn()
    {
        if (rigidbody.gravityScale < 0)
        {
            GravitySwitch();
        }
    }
}
