using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Transform agent;
    private Vector2 startPosition;
    private float startZ;
    private Vector2 travel => (Vector2)cam.transform.position - startPosition;
    private float distanceFromAgent => transform.position.z - agent.position.z;
    private float clippingPlane => cam.transform.position.z + cam.farClipPlane;
    private float parallaxFactor => Mathf.Abs(distanceFromAgent / clippingPlane);
    
    void Start()
    {
        startPosition = transform.position;
        startZ = transform.position.z;
    }
    void Update()
    {
        Vector2 newPosition = startPosition + travel * parallaxFactor;
        transform.position = new Vector3(newPosition.x, newPosition.y, startZ);
    }

}
