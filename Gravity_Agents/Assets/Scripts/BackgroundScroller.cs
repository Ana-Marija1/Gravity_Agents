using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    [Range(0f, 1f)]
    public float speed = 1f;
    private float offset;
    private Material material;
    void Start()
    {
        material = GetComponent<Renderer>().material;
    }
    void Update()
    {
        offset += (Time.deltaTime * speed) / 10f;
        material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}
