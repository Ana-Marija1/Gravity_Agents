using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapManager : MonoBehaviour
{
    [SerializeField] private List<Transform> traps;
    public void SetTrapsTraining()
    {
        traps[0].localPosition = new Vector3(Random.Range(-9f, -1.5f), -5.5f);
        traps[1].localPosition = new Vector3(Random.Range(-15f, -10f), 5.5f);
        traps[2].localPosition = new Vector3(Random.Range(17.5f, 20f), -2.5f);
        traps[3].localPosition = new Vector3(Random.Range(21.5f, 24f), 2.5f);
    }
    public void SetTrapsLevel()
    {
        traps[0].localPosition = new Vector3(Random.Range(-9f, -1.5f), -5.5f);
        traps[1].localPosition = new Vector3(Random.Range(-15f, -10f), 5.5f);
        traps[2].localPosition = new Vector3(Random.Range(193.5f, 196f), -2.5f);
        traps[3].localPosition = new Vector3(Random.Range(197.5f, 200f), 2.5f);
    }
}
