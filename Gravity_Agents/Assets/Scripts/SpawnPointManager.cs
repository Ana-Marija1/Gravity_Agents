using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointManager : MonoBehaviour
{
    [SerializeField] private List<Transform> spawnPoints;
    public Vector3 ChooseSpawnPoint()
    {
        return spawnPoints[Random.Range(0, spawnPoints.Count)].localPosition;
    }
}
