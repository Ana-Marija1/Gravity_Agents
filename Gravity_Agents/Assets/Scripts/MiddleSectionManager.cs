using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleSectionManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> middleSections;
    private int chosenMiddleSection;
    private static readonly System.Random random = new System.Random();

    public void ChooseMiddleSection()
    {
        for (int i = 0; i < middleSections.Count; i++)
        {
            middleSections[i].SetActive(false);
        }
        if (middleSections.Count == 15)
        {
            Shuffle(middleSections);
            for (int i = 0; i < 12; i++)
            {
                middleSections[i].transform.localPosition = new Vector3(i * 16, 0);
                middleSections[i].SetActive(true);
            }

        }
        else
        {
            chosenMiddleSection = Random.Range(0, middleSections.Count);
            middleSections[chosenMiddleSection].SetActive(true);
        }
    }
    private List<GameObject> Shuffle(List<GameObject> middleSectionsToShuffle)
    {
        for (int i = middleSectionsToShuffle.Count - 1; i > 0; i--)
        {
            int randNum = random.Next(i + 1);
            GameObject temp = middleSectionsToShuffle[randNum];
            middleSectionsToShuffle[randNum] = middleSectionsToShuffle[i];
            middleSectionsToShuffle[i] = temp;
        }
        return middleSectionsToShuffle;
    }
}
