using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IScenes
{
    enum  Name{
        MainMenu,
        Level_1,
        Level_2,
        Level_3,
        TrainingLevel,
        SampleScene
    };
}

