using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine.SceneManagement;
public class GravityAgent : Agent, IScenes, IGravity
{
    [SerializeField] private SpriteRenderer backgroundSpriteRenderer;
    [SerializeField] private Transform target;
    [SerializeField] private LayerMask platformLayer;
    [SerializeField] private SpawnPointManager spawnPointManager;
    [SerializeField] private MiddleSectionManager middleSectionManager;
    [SerializeField] private TrapManager trapManager;
    private new Rigidbody2D rigidbody;
    private BoxCollider2D boxCollider;
    private bool top = false;
    private bool facingRight = true;
    public override void OnEpisodeBegin()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        if (SceneManager.GetActiveScene().name == IScenes.Name.SampleScene.ToString())
        {
            trapManager.SetTrapsTraining();
            target.localPosition = new Vector3(Random.Range(26, 31), 0);
        }
        else
        {
            trapManager.SetTrapsLevel();
            target.localPosition = new Vector3(Random.Range(202, 207), 0);
        }
        middleSectionManager.ChooseMiddleSection();
        Spawn();
        transform.localPosition = spawnPointManager.ChooseSpawnPoint();
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation((Vector2)transform.localPosition);
        sensor.AddObservation((Vector2)target.localPosition);
    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.DiscreteActions[0];
        float changeGravity = actions.DiscreteActions[1];
        float speed = 5f;

        if (moveX == 1)
        {
            AddReward(-0.01f);
            if (facingRight)
            {
                Flip();
            }
            transform.localPosition += Vector3.left * speed * Time.deltaTime;
        }
        else if (moveX == 2)
        {
            AddReward(0.01f);
            if (!facingRight)
            {
                Flip();
            }
            transform.localPosition += Vector3.right * speed * Time.deltaTime;
        }
        else if (changeGravity == 1)
        {
            GravitySwitch();
        }
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<int> discreteActions = actionsOut.DiscreteActions;
        if (IsGrounded())
        {
            if (Input.GetKey(KeyCode.A))
            {
                discreteActions[0] = 1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                discreteActions[0] = 2;
            }
            discreteActions[1] = Input.GetKey(KeyCode.W) ? 1 : 0;
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Target"))
        {
            if (SceneManager.GetActiveScene().name == IScenes.Name.SampleScene.ToString() || SceneManager.GetActiveScene().name == IScenes.Name.TrainingLevel.ToString())
            {
                AddReward(2f);
                backgroundSpriteRenderer.color = Color.green;
                EndEpisode();
            }
            else
            {
                EndEpisode();
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        if (collider.gameObject.CompareTag("Border") || collider.gameObject.CompareTag("Trap"))
        {
            if (SceneManager.GetActiveScene().name == IScenes.Name.SampleScene.ToString() || SceneManager.GetActiveScene().name == IScenes.Name.TrainingLevel.ToString())
            {
                AddReward(-2f);
                backgroundSpriteRenderer.color = Color.red;
            }
            EndEpisode();
        }
    }
    public void Reset()
    {
        EndEpisode();
    }
    public bool IsGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, 0.4f, platformLayer);
        if (top == true)
        {
            raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.up, 0.4f, platformLayer);
        }
        return raycastHit.collider != null;
    }
    public void GravitySwitch()
    {
        rigidbody.gravityScale *= -1;
        Rotate();
        facingRight = !facingRight;
        Flip();
    }
    public void Rotate()
    {
        transform.localEulerAngles = top ? Vector3.zero : new Vector3(0, 0, 180f);
        top = !top;
    }
    public void Flip()
    {
        facingRight = !facingRight;
        if (top == false)
        {
            transform.localEulerAngles = facingRight ? Vector3.zero : new Vector3(0, 180f, 0);
        }
        else
        {
            transform.localEulerAngles = facingRight ? new Vector3(0, 180f, 180f) : new Vector3(0, 0, 180f);
        }
    }
    public void Spawn()
    {
        if (rigidbody.gravityScale < 0)
        {
            GravitySwitch();
        }
    }
}
